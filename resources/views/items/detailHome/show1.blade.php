@extends('adminlte.master')

@section('judulFile')
  Project
@endsection

@section('judul1')
Show Off Project ESD Study Group
@endsection

@section('judul2')
Booking Wedding Online
@endsection

@section('isi')
  <div>
  <img class="card-img-top"  src="{{asset('adminlte/dist/img/0.png')}}" alt="Card image cap">
  <br> <br>
  <h5 class="font-weight-bold">Penjelasan</h5>
  <p>Dengan Gendhis Wedding, user akan dimudahkan dalam persiapan pernikahan</p>

  <h5 class="font-weight-bold">Tools Yang digunakan</h5>
  <p>Figma, Visual Studio Code, Boostrap CSS, Laravel, PHP</p>
  </div>

  <div class="card-body">
    <div class="media">
      <img alt="" class="img-circle mr-3" id="practitioner_image" src="{{asset('adminlte/dist/img/blank.png')}}" class="main_badge" title="" style="height:50px; weight:50px;">
      <div class="media-body">
        <a class="mt-0 font-weight-normal">Muhammad Bima</a>
        <p class="font-weight-light">UI/UX Study Group</p>
      </div>
    </div>
@endsection