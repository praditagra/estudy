@extends('adminlte.master')

@section('judulFile')
  Halaman Latihan
@endsection

@section('judul1')
  Latihan
@endsection

@section('judul2')
  Studi Kasus Software Developer
@endsection

@section('isi')
    <div class = "row">
      <div class = "col-4">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" style="height: 10rem;" src="{{asset('adminlte/dist/img/ilustrasi case study/airplane.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Airplane App</h5>
            <p class="card-text font-weight-light">Dibutuhkan aplikasi mobile yang dapat mempermudah user dalam booking seat pesawat tanpa perlu repot mengantri.</p>
            <a href="#" class="btn btn-success btn-block">Detail Study Case</a>
          </div>
        </div>
      </div>  

      <div class = "col-4">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" style="height: 10rem;" src="{{asset('adminlte/dist/img/ilustrasi case study/onlinecourse.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Online Course Web</h5>
            <p class="card-text font-weight-light">Pada Study Case ini, kamu harus membuat web yang diperuntukkan untuk orang yang mau memperdalam mengenai Coding.</p>
            <a href="#" class="btn btn-success btn-block">Detail Study Case</a>
          </div>
        </div>
      </div>  


      <div class = "col-4">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" style="height: 10rem;" src="{{asset('adminlte/dist/img/ilustrasi case study/shop.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Aplikasi Self Service belanja</h5>
            <p class="card-text font-weight-light">Bagaimana ya cara membuat mobile apps untuk orang yang ingin melakukan selfservice saat berbelanja?</p>
            <a href="#" class="btn btn-success btn-block">Detail Study Case</a>
          </div>
        </div>
      </div>  


    </div>

</div>
@endsection

@section('isi2')
<div class="card mt-5">
        <div class="card-header">
          <h1 class="card-title">Study Kasus UI UX Designer</h1>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
          </div>
        </div>
        <div class="card-body">
        <div class = "row">
        <div class = "col-4">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" style="height: 10rem;" src="{{asset('adminlte/dist/img/ilustrasi case study/motor.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Aplikasi Pelacak Rental motor</h5>
            <p class="card-text font-weight-light">Dibutuhkan aplikasi mobile yang dapat mempermudah user dalam booking seat pesawat tanpa perlu repot mengantri.</p>
            <a href="#" class="btn btn-success btn-block">Detail Study Case</a>
          </div>
        </div>
      </div>  

      <div class = "col-4">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" style="height: 10rem;" src="{{asset('adminlte/dist/img/ilustrasi case study/wash car.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Aplikasi tempat cuci mobil</h5>
            <p class="card-text font-weight-light">Ketika seseorang pergi ke tempat cuci mobil, tentu saja ada kemungkinan tempat cuci mobil tersebut sangat ramai.</p>
            <a href="#" class="btn btn-success btn-block">Detail Study Case</a>
          </div>
        </div>
      </div>  


      <div class = "col-4">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" style="height: 10rem;" src="{{asset('adminlte/dist/img/ilustrasi case study/time.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Aplikasi Pengelola waktu</h5>
            <p class="card-text font-weight-light">Hmmm... Time is money, sepertinya dibutuhkan aplikasi pengelola waktu agar hari jadi lebih teratur. Mari kita buat!</p>
            <a href="#" class="btn btn-success btn-block">Detail Study Case</a>
          </div>
        </div>
      </div>  


    </div>

</div>
        </div>
@endsection

@section('isi3')
<div class="card mt-5">
        <div class="card-header">
          <h1 class="card-title">Study Kasus Techno Preneur</h1>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
          </div>
        </div>
        <div class="card-body">
        <div class = "row">
        <div class = "col-4">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" style="height: 10rem;" src="{{asset('adminlte/dist/img/ilustrasi case study/usaha.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Wirausaha & Teknologi</h5>
            <p class="card-text font-weight-light">Dibutuhkan aplikasi mobile yang dapat mempermudah user dalam booking seat pesawat tanpa perlu repot mengantri.</p>
            <a href="#" class="btn btn-success btn-block">Detail Study Case</a>
          </div>
        </div>
      </div>  

      <div class = "col-4">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" style="height: 10rem;" src="{{asset('adminlte/dist/img/ilustrasi case study/onlinecourse.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Online Course Web</h5>
            <p class="card-text font-weight-light">Pada Study Case ini, kamu harus membuat web yang diperuntukkan untuk orang yang mau memperdalam mengenai Coding.</p>
            <a href="#" class="btn btn-success btn-block">Detail Study Case</a>
          </div>
        </div>
      </div>  


 

    </div>

</div>
        </div>
@endsection