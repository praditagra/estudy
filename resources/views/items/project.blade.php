@extends('adminlte.master')

@section('judulFile')
  Halaman Project
@endsection

@section('judul1')
  Project
@endsection

@section('judul2')
  Project List
@endsection

@section('isi')
    <div class = "row">
      <div class = "col">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" src="{{asset('adminlte/dist/img/11.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Project Riset dan Build Aplikasi Travel Online</h5>
            <p class="card-text font-weight-light">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            
            <div class="media">
              <img alt="" class="img-circle mr-3" id="practitioner_image" src="{{asset('adminlte/dist/img/user4-128x128.jpg')}}" class="main_badge" title="" style="height:50px; weight:50px;">
              <div class="media-body">
                <a class="mt-0 font-weight-normal">Pradita</a>
                <p class="font-weight-light">Tabita Project</p>
              </div>
            </div>
            <a href="#" class="btn btn-success text-right" style="float: right;">Detail Project</a>
          </div>
        </div>
      </div>  

      <div class = "col">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" src="{{asset('adminlte/dist/img/22.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Project Riset Memenuhi kebutuhan Mahasiswa</h5>
            <p class="card-text font-weight-light">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            
            <div class="media">
              <img alt="" class="img-circle mr-3" id="practitioner_image" src="{{asset('adminlte/dist/img/user7-128x128.jpg')}}" class="main_badge" title="" style="height:50px; weight:50px;">
              <div class="media-body">
                <a class="mt-0 font-weight-normal">Cahya</a>
                <p class="font-weight-light">Tabita Project</p>
              </div>
            </div>
            <a href="#" class="btn btn-success text-right" style="float: right;">Detail Project</a>
          </div>
        </div>
      </div>  

      <div class = "col">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" src="{{asset('adminlte/dist/img/55.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Project Junior Front End Developer</h5>
            <p class="card-text font-weight-light">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            
            <div class="media">
              <img alt="" class="img-circle mr-3" id="practitioner_image" src="{{asset('adminlte/dist/img/user1-128x128.jpg')}}" class="main_badge" title="" style="height:50px; weight:50px;">
              <div class="media-body">
                <a class="mt-0 font-weight-normal">Rizal</a>
                <p class="font-weight-light">Tabita Project</p>
              </div>
            </div>
            <a href="#" class="btn btn-success text-right" style="float: right;">Detail Project</a>
          </div>
        </div>
      </div>  

      <div class = "col">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" src="{{asset('adminlte/dist/img/44.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Project Riset Aplikasi Penyuluhan sampah</h5>
            <p class="card-text font-weight-light">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            
            <div class="media">
              <img alt="" class="img-circle mr-3" id="practitioner_image" src="{{asset('adminlte/dist/img/user8-128x128.jpg')}}" class="main_badge" title="" style="height:50px; weight:50px;">
              <div class="media-body">
                <a class="mt-0 font-weight-normal">Rizal Bima</a>
                <p class="font-weight-light">Tabita Project</p>
              </div>
            </div>
            <a href="#" class="btn btn-success text-right" style="float: right;">Detail Project</a>
          </div>
        </div>
      </div> 

      <div class = "col">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" src="{{asset('adminlte/dist/img/33.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Project Junior Technopreneur Analysis</h5>
            <p class="card-text font-weight-light">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            
            <div class="media">
              <img alt="" class="img-circle mr-3" id="practitioner_image" src="{{asset('adminlte/dist/img/user6-128x128.jpg')}}" class="main_badge" title="" style="height:50px; weight:50px;">
              <div class="media-body">
                <a class="mt-0 font-weight-normal">Toro Biman</a>
                <p class="font-weight-light">Tabita Project</p>
              </div>
            </div>
            <a href="#" class="btn btn-success text-right" style="float: right;">Detail Project</a>
          </div>
        </div>
      </div> 

      

    </div>
@endsection