<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('items.welcome');
});
Route::get('/login', function () {
    return view('items.login');
});
Route::get('/home', function () {
    return view('items.home');
});
Route::get('/materi', function () {
    return view('items.materi');
});
Route::get('/latihan', function () {
    return view('items.latihan');
});

Route::get('/project', function () {
    return view('items.project');
});

Route::get('/detailMateri1', function () {
    return view('items.detailMateri.detailMateri1');
});

Route::get('/detailMateri2', function () {
    return view('items.detailMateri.detailMateri2');
});
Route::get('/detailMateri3', function () {
    return view('items.detailMateri.detailMateri3');
});
Route::get('/detailMateri4', function () {
    return view('items.detailMateri.detailMateri4');
});
Route::get('/detailMateri5', function () {
    return view('items.detailMateri.detailMateri5');
});
Route::get('/detailMateri6', function () {
    return view('items.detailMateri.detailMateri6');
});
Route::get('/show1', function () {
    return view('items.detailHome.show1');
});
Route::get('/show2', function () {
    return view('items.detailHome.show2');
});
Route::get('/show3', function () {
    return view('items.detailHome.show3');
});

Route::get('/mentoring/create','ESDController@create');
Route::post('/mentoring','ESDController@store');
Route::get('/mentoring','ESDController@index');
Route::get('/mentoring/{id}','ESDController@show');
Route::get('/mentoring/{id}/edit','ESDController@edit');
Route::put('/mentoring/{id}','ESDController@update');
Route::delete('/mentoring/{id}','ESDController@destroy');