<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ESDController extends Controller
{
    public function create(){
        return view('mentoring.create');
    }

    public function store(Request $request){
        $request->validate([
            "nama"=> 'required|unique:mentoring',
            "deskripsi"=> 'required',
            "link"=> 'required'
        ]);

        $query = DB::table('mentoring')->insert([            
            "nama" => $request["nama"],
            "deskripsi" => $request["deskripsi"],
            "link" => $request["link"]
        ]);
        
        return redirect('mentoring')->with('berhasil','Project berhasil ditambahkan');
    }

    public function index() {
        $mentoring = DB::table('mentoring')->get();
        return view('mentoring.index',compact('mentoring'));
    }

    public function show($id){
        $mentoring = DB::table('mentoring')->where('id',$id)->first();
        return view('mentoring.show',compact('mentoring'));
    }

    public function edit($id){
        $mentoring = DB::table('mentoring')->where('id',$id)->first();

        return view('mentoring.edit',compact('mentoring'));
    }

    public function update($id,Request $request){
        $request->validate([
            "nama"=> 'required',
            "deskripsi"=> 'required',
            "link"=> 'required'
        ]);

        $query = DB::table('mentoring')
                    ->where('id',$id)
                    ->update([
                        'nama' => $request['nama'],
                        'deskripsi' => $request['deskripsi'],
                        'link' => $request['link']
                    ]);
        return redirect('/mentoring')->with('berhasil','Berhasil update data Project');
    }

    public function destroy($id) {
        $query = DB::table('mentoring')->where('id', $id)->delete();
        return redirect('mentoring')->with('berhasil','Data project berhasil dihapus');

    }
}
